## Installing i3wm

1. sudo apt update
2. sudo apt install i3
3. sudo apt-get update && sudo apt-get install i3-wm i3status i3lock suckless-tools

---

## Basic Configuration

1. mkdir ~/.i3
2. sudo cp /etc/i3/config ~/.i3/config
3. sudo chown user:group ~/.i3/config

The configuration file is present in the repository 

---

## Edit theme
To edit i3 theme you need to install lxappearance

1. sudo apt-get update
2. sudo apt-get upgrade
3. sudo apt-get install lxappearance

then run lxappearance tool